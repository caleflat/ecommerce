module github.com/caleflat/ecommerce

go 1.21.1

require (
	github.com/jmoiron/sqlx v1.3.5
	github.com/labstack/echo v3.3.10+incompatible
	go.uber.org/automaxprocs v1.5.3
	go.uber.org/zap v1.26.0
)

require (
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/mattn/go-colorable v0.1.11 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.1 // indirect
	go.uber.org/multierr v1.10.0 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
)
