package log

import (
	"io/fs"
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// NewAtLevel creates a new logger at the specified level.
// If levelStr is empty, the default level is Info.
func NewAtLevel(levelStr string) (*zap.Logger, error) {
	logLevel := zapcore.InfoLevel
	if levelStr != "" {
		var err error
		logLevel, err = zapcore.ParseLevel(levelStr)
		if err != nil {
			return nil, err
		}
	}

	logConf := zap.NewProductionConfig()
	logConf.Level = zap.NewAtomicLevelAt(logLevel)
	logConf.OutputPaths = []string{"stdout", "logs/app.log"}

	// Create logs dir if it doesn't exist
	if _, err := os.Stat("logs"); os.IsNotExist(err) {
		err := os.Mkdir("logs", fs.ModePerm)
		if err != nil {
			return nil, err
		}
	}

	logger, err := logConf.Build()
	if err != nil {
		return nil, err
	}

	return logger, nil
}
