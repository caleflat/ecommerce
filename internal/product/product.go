package product

// Product represents a product stored in a database.
type Product struct {
	ID          int     `json:"id"`
	Title       string  `json:"title"`
	Description *string `json:"description"`
	Images      []Image `json:"images"`
}

// Image represents an image stored in a database.
type Image struct {
	ID     int    `json:"id"`
	Source string `json:"source"`
	Alt    string `json:"alt"`
}
