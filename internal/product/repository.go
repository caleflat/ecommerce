package product

import (
	"context"

	"github.com/jmoiron/sqlx"
)

type Repository interface {
	GetProduct(ctx context.Context, id int) (*Product, error)
	GetProducts(ctx context.Context, limit int, offset int) ([]*Product, error)
}

func NewRepository(db *sqlx.DB) Repository {
	return &repository{db}
}

type repository struct {
	db *sqlx.DB
}

func (r *repository) GetProduct(ctx context.Context, id int) (*Product, error) {
	query := `
	SELECT * FROM products
	WHERE id = $1
	`
	row := r.db.QueryRowxContext(ctx, query, id)
	if row.Err() != nil {
		return nil, row.Err()
	}

	var product Product
	err := row.StructScan(&product)
	if err != nil {
		return nil, err
	}

	return &product, nil
}

func (r *repository) GetProducts(ctx context.Context, limit int, offset int) ([]*Product, error) {
	query := `
	SELECT * FROM products
	LIMIT $1
	OFFSET $2
	`
	rows, err := r.db.QueryxContext(ctx, query, limit, offset)
	if err != nil {
		return nil, err
	}

	var products []*Product
	for rows.Next() {
		var product Product
		if err := rows.Scan(&product); err != nil {
			return nil, err
		}

		products = append(products, &product)
	}

	return nil, nil
}
